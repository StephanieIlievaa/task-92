import Card from "./Card.js";
import classNames from "classnames";
import { formatCurrency } from "./utils.js"


export default class Notification {
  static get types() {
    return {
      PEPPERONI: "pepperoni",
      MARGHERITA: "margherita",
      HAWAIIAN: "hawaiian",
    };
  }

  constructor() {
    this.container = document.createElement("div");
    this.container.classList.add("notification-container");
    this.notificationEl = document.querySelector('.notifications');
  }

  render({ type, price }) {
    const template = `
<div class="notification type-${type} ${classNames({ "is-danger": type === Card.types.HAWAIIAN })}">
  <button class="delete"></button>
  🍕 <span class="type">${type}</span> (<span class="price">${formatCurrency(price)}</span>) has been added to your order.
</div>
    `;

    this.container.innerHTML = template;
//notification display
    this.notificationEl.appendChild(this.container);

    let btn = this.container.querySelector('.delete');
    btn.addEventListener('click', () => this.empty());
  }
  empty() {
    this.notificationEl.removeChild(this.container);
  }
}
