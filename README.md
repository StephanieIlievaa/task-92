# ⚙ HTML, CSS and SASS Boilerplate

This project is used as a boilerplate for tasks in the "HTML, CSS and SASS" course in boom.dev

🤯💥💣

# A JavaScript Task

## Objective
- Checkout the dev branch
- Create a notification each time a card is clicked
- When implemented merge dev branch into master.

## Requirements
- The project starts with **npm run start**.
- There must be a card for each pizza
- A notification must be created and added to the page
- The notification must display the pizza type and the pizza price
- The notification must have a **"is-danger"** class if the type is hawaii.
- THe notification must be closed when the button is clicked
- The notification must import **formatCurrency** method from utils.
- The notification must import the classNames module. You can read more about it here - https://www.npmjs.com/package/classnames
- The notification must have a empty method whitch clears the HTML contents of the container
- The render method must have a price argument
- The render method must have a type argument
- The classNames and formatCurrency modules must be used in render().
